<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Service;

use matricula\Core\Template;
use matricula\Model\DocenteModel;
use context_system;
use moodle_url;
use stdClass;
use matricula\Model\UtilModel;

class DocenteService extends Template {

    private $valid_exts = ['pdf', 'PDF'];
    private $max_size = 200000 * 1024;
    private $path_certificado;
    private $utilService;

    public function __construct() {
        $this->valid_exts = ['pdf', 'PDF'];
    }

    public static function getDocentesAll() {
        return DocenteModel::getDocentesAll();
    }

    public static function getCursosAllDesocupados() {
        return DocenteModel::getCursosAllDesocupados();
    }

    public static function getCursosAllDesocupadosDirector($id) {
        return DocenteModel::getCursosAllDesocupadosDirector($id);
    }

    public static function getDocenteById($id) {
        return DocenteModel::getDocenteById($id);
        ;
    }

    public static function getDocenteByDNI($dni) {
        return DocenteModel::getDocenteByDNI($dni);
        ;
    }

    public static function getDocenteByCorreo($dni) {
        return DocenteModel::getDocenteByCorreo($dni);
        ;
    }

    public static function getDocenteByCelular($dni) {
        return DocenteModel::getDocenteByCelular($dni);
        ;
    }

    public static function getNuevoDocente() {
        global $USER;
        $obj = new \stdClass();
        //activo
        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        $obj->int_creatorid = $USER->id;
        $obj->id = DocenteModel::saveDocente($obj);
        return $obj;
    }

    public function getUriEditDocente($alumnoid) {
        return $this->routes()->generate('docentes_editar', ['docenteid' => $alumnoid]);
    }

    public static function eliminarDocenteCurso($id) {
        DocenteModel::updateDocenteCurso($id);
        DocenteModel::updateCursoByCursoid($id);
        return $id;
    }

    public static function eliminarDocente($id) {
        global $USER;
        $obj = new \stdClass();
        //activo
        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        $obj->int_creatorid = $USER->id;
        $obj->id = $id;
        DocenteModel::updateDocente($obj);
        DocenteModel::updateCursoByDecenteid($id);
        DocenteModel::updateDocenteCursoByDecenteid($id);
        return $obj;
    }

    public function GuardarDocenteCurso($iddocente, $idcursos) {
        $array = explode(",", $idcursos);
        $temporal = CursoService::getCursosById($idcursos);
        $noprocede = 0;
        foreach ($temporal as $curso) {
            foreach ($temporal as $temporal) {
                if ($curso->chr_dia == $temporal->chr_dia && $curso->chr_horainicio == $temporal->chr_horainicio && $curso->chr_horafin == $temporal->chr_horafin && $curso->id != $temporal->id) {
                    $noprocede = 1;
                }
            }
        }
        if ($noprocede == 0) {
            global $USER;
            foreach ($array as $curso) {
                $obj = new \stdClass();
                $obj->int_docenteid = $iddocente;
                $obj->int_cursoid = $curso;
                //activo
                $obj->is_active = 1;
                //eliminado
                $obj->is_deleted = 0;
                $obj->int_creatorid = $USER->id;
                $obj->date_timecreated = time();
                DocenteModel::saveDocenteCurso($obj);
                $obj2 = new \stdClass();
                $obj2->id = $curso;
                $obj2->is_ocupado2 = 1;
                \matricula\Model\CursoModel::updateCurso($obj2);
            }
        }

        return $noprocede;
    }

    public function GuardarDocente($inputs) {
        $id = $inputs->get('docenteid');
        global $USER;
        $obj = new \stdClass();
        //id
        $obj->id = $inputs->get('docenteid');
        //nombre
        $obj->chr_first_name = $inputs->get('inputTxtName');
        $obj->chr_first_name = strip_tags($obj->chr_first_name);
        $obj->chr_first_name = trim($obj->chr_first_name);
        //apeliido
        $obj->chr_last_name = $inputs->get('inputTxtApellido');
        $obj->chr_last_name = strip_tags($obj->chr_last_name);
        $obj->chr_last_name = trim($obj->chr_last_name);
        //dni
        $obj->int_dni = $inputs->get('inputTxtDNI');
        //fecha ingreso
        $obj->chr_fecha_nacimiento = $inputs->get('inputTxtFechaDeNacimiento');
        //correo
        $obj->chr_correo = $inputs->get('inputTxtcorreo');
        //celular
        $obj->int_celular = $inputs->get('inputTxtcelular');
        //activo
        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        if ($id > 0) {
            //fecha modificacion registro
            $obj->date_timemodified = time();
            $returnValue = DocenteModel::updateDocente($obj);
        } else {
            //fecha creacion registro
            $obj->date_timecreated = time();
            $returnValue = DocenteModel::saveDocente($obj);
        }


        $returnValue = $inputs->get('docenteid');
        return $returnValue;
    }

}
