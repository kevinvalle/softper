<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Service;

use matricula\Core\Template;
use matricula\Model\MatriculaModel;
use matricula\Model\AlumnoModel;
use context_system;
use moodle_url;
use stdClass;
use matricula\Model\UtilModel;

class MatriculaService extends Template {

    private $valid_exts = ['pdf', 'PDF'];
    private $max_size = 200000 * 1024;
    private $path_certificado;
    private $utilService;

    public function __construct() {
        $this->valid_exts = ['pdf', 'PDF'];
    }

    public static function getMatriculasAll() {
        return MatriculaModel::getMatriculasAll();
        ;
    }

    public static function getAllCompraProductoByCompraid($id) {
        return MatriculaModel::getAllCompraProductoByCompraid($id);
        ;
    }

    public static function getAllComprasByCreditoCliente($id) {
        return MatriculaModel::getAllComprasByCreditoCliente($id);
        ;
    }

    public static function getFechaDiasActual() {
        return MatriculaModel::getFechaDiasActual();
        ;
    }

    public static function getAllPagosByCreditoCliente($id) {
        return MatriculaModel::getAllPagosByCreditoCliente($id);
        ;
    }

    public static function getAllTotalCompraProductoByCompraid($id) {
        return MatriculaModel::getAllTotalCompraProductoByCompraid($id);
        ;
    }

    public static function getMatriculasAllSemestre($id) {
        return MatriculaModel::getMatriculasAllSemestre($id);
        ;
    }

    public static function getSemestresAll() {
        return MatriculaModel::getSemestresAll();
        ;
    }

    public static function getNuevaMatricula() {
        global $USER;
        $obj = new \stdClass();
        //activo
        $obj->is_active = 0;
        $obj->int_clienteid = 0;
        $obj->int_credito_monto = 0;
        $obj->int_pagado_monto = 0;
        $obj->int_debe_monto = 0;
        $obj->int_disponible = 0;


        //eliminado
        $obj->is_deleted = 1;
        $obj->int_creatorid = $USER->id;
        $obj->date_timecreated = time();
        $obj->id = MatriculaModel::saveMatricula($obj);
        return $obj;
    }

    public static function getMatriculaById($id) {
        return MatriculaModel::getMatriculaById($id);
        ;
    }

    public static function getAllMovimientos($id) {
        return MatriculaModel::getAllMovimientos($id);
        ;
    }

    public static function getCompraById($id) {
        return MatriculaModel::getCompraById($id);
    }

    public static function getPagoById($id) {
        return MatriculaModel::getPagoById($id);
    }

    public static function getCompraProductoById($id) {
        return MatriculaModel::getCompraProductoById($id);
        ;
    }

    public static function getCursosAllDisponibles($objMatricula) {
        return MatriculaModel::getCursosAllDisponibles($objMatricula);
        ;
    }

    public static function getCursosAllSeleccionados($objMatricula) {
        return MatriculaModel::getCursosAllSeleccionados($objMatricula);
        ;
    }

    public function getUriEditMatricula($alumnoid) {
        return $this->routes()->generate('matriculas_editar', ['matriculaid' => $alumnoid]);
    }

    public static function siguientedia($dia) {
        $obj = MatriculaModel::getFechaActual();
        $obj->chr_dia = $obj->chr_dia + $dia;
        $mesactual =$obj->chr_mes;
        //activo
      
        if ($obj->chr_dia >= 31) {
            $obj->chr_mes = '0'.($obj->chr_mes + 1);
            $obj->chr_dia = $obj->chr_dia - 30;
        }
        //eliminado
        if ($obj->chr_mes >=13) {
            $obj->chr_anio = $obj->chr_anio + 1;
            $obj->chr_mes = '01';
        }
        
        if ($obj->chr_dia < 10) {
            $obj->chr_dia = '0' . $obj->chr_dia;
        }
        
        if ($obj->chr_mes < 10) {
            $obj->chr_mes = $obj->chr_mes;
        }
        $obj->chr_fecha_actual = $obj->chr_dia . '/' . $obj->chr_mes . '/' . $obj->chr_anio;

        $creditos_cliente = MatriculaModel::getCreditoClientesAll();
        
        foreach ($creditos_cliente as $index) {
            if ($obj->chr_dia == $index->chr_dia||$mesactual<  $obj->chr_mes ) {
                $index->int_mantenimiento = $index->int_mantenimiento + 1;
                $index->int_mantenimiento = round($index->int_mantenimiento,2);
                $index->int_debe_monto = $index->int_debe_monto + 1;
                $index->int_debe_monto = round($index->int_debe_monto,2);
                $index->int_disponible = $index->int_disponible - 1;
                $index->int_disponible = round( $index->int_disponible,2);
            }
            $COMPRAS = MatriculaModel::getAllComprasByCreditoCliente($index->id);
            foreach ($COMPRAS as $COMPRA) {
                $interes = 0;
                if($COMPRA->int_total>0){
                    //kevin formula de interes 
                    $sinselivery=0;
                    $sinselivery=($COMPRA->int_total-$COMPRA->int_delivery);
                    $interes = ( $sinselivery* (pow((1 + $index->int_tasa_efectiva), $dia) - 1));
                    
                }
//                $COMPRA->int_compra = $COMPRA->int_compra + $interes;
//                $COMPRA->int_compra = round( $COMPRA->int_compra,2);
                $COMPRA->int_total = $COMPRA->int_total + $interes;
                $COMPRA->int_total = round( $COMPRA->int_total,2);
                $COMPRA->int_interes_generado = $COMPRA->int_interes_generado + $interes;
                $COMPRA->int_interes_generado = round( $COMPRA->int_interes_generado,2);
                $index->int_debe_monto = $index->int_debe_monto + $interes;
                $index->int_debe_monto = round( $index->int_debe_monto,2);
                
                MatriculaModel::updateCompra($COMPRA);
            }
            $index->int_dias = $index->int_dias - $dia;
            $index->int_tasa_efectiva = (pow((1 + (($index->int_interes / 100) / 30)), $index->int_dias) - 1);
            
            if($index->int_tasa_efectiva <=0){
               $index->int_tasa_efectiva = $index->int_tasa_efectiva = (pow((1 + (($index->int_interes / 100) / 30)), $index->int_dias_totales) - 1);;
               $index->int_dias = $index->int_dias_totales;
            }
            
            MatriculaModel::updateMatricula($index);
        }

        return MatriculaModel::updateDia($obj);
    }

    public function eliminarPago($idmatricula) {
        //alumo libre
        //matricula
        $obj = new \stdClass();
        $obj->id = $idmatricula;
        //activo
        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        
        MatriculaModel::updatePago($obj);
        $compra = MatriculaModel::getPagoById($idmatricula);
        $credito = MatriculaModel::getMatriculaById($compra->int_credito_clienteid);
        $obj2 = new \stdClass();
        $obj2->id = $credito->id;
        $obj2->int_disponible = $credito->int_disponible - $compra->int_total;
        $obj2->int_disponible  = round( $obj2->int_disponible ,2);
        $obj2->int_pagado_monto = $credito->int_pagado_monto + $compra->int_total;
        $obj2->int_pagado_monto  = round( $obj2->int_pagado_monto ,2);
        $obj2->int_debe_monto = $obj2->int_debe_monto + $compra->int_total;
         $obj2->int_debe_monto  = round( $obj2->int_debe_monto ,2);
        MatriculaModel::updateMatricula($obj2);
        return $idmatricula;
    }

    public function eliminarCompra($idmatricula) {
        //alumo libre
        //matricula
        $obj = new \stdClass();
        $obj->id = $idmatricula;
        //activo
        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        MatriculaModel::updateCompra($obj);

        $compra = MatriculaModel::getCompraById($idmatricula);
        $credito = MatriculaModel::getMatriculaById($compra->int_credito_clienteid);
        $obj2 = new \stdClass();
        $obj2->id = $credito->id;
        $obj2->int_disponible = $credito->int_disponible + $compra->int_total;
        $obj2->int_disponible  = round( $obj2->int_disponible ,2);
        $obj2->int_gastado = $credito->int_gastado - $compra->int_total;
        $obj2->int_gastado  = round( $obj2->int_gastado ,2);
        $obj2->int_debe_monto = $obj2->int_debe_monto - $compra->int_total;
        $obj2->int_debe_monto  = round( $obj2->int_debe_monto ,2);
        MatriculaModel::updateMatricula($obj2);
        return $idmatricula;
    }

    public function eliminarCompraMatricula($idmatricula) {
        //alumo libre
        //matricula
        $obj = new \stdClass();
        $obj->id = $idmatricula;
        //activo
        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        MatriculaModel::updateCompraProducto($obj);
        return $idmatricula;
    }

    public function eliminarMatricula($idmatricula) {
        //alumo libre
        MatriculaModel::UpdateAlumnoOcupado($idmatricula);
        //matricula
        $obj = new \stdClass();
        $obj->id = $idmatricula;
        //activo
        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        MatriculaModel::updateMatricula($obj);
        return $idmatricula;
    }

    public function eliminarMatriculaCurso($idcurso, $idmatricula) {
        MatriculaModel::updateMatriculaCurso($idmatricula, $idcurso);
        $obj2 = \matricula\Model\CursoModel::getCursoById($idcurso);
        $obj2->int_cantidad_ocupada = $obj2->int_cantidad_ocupada - 1;
        $obj2->is_ocupado = 0;
        \matricula\Model\CursoModel::updateCurso($obj2);
        return $idmatricula;
    }

    public function GuardarMatriculaCurso($idmatricula, $idcursos) {
        $array = explode(",", $idcursos);
        $temporal = CursoService::getCursosById($idcursos);
        $noprocede = 0;
        foreach ($temporal as $curso) {
            foreach ($temporal as $temporal) {
                if ($curso->chr_dia == $temporal->chr_dia && $curso->chr_horainicio == $temporal->chr_horainicio && $curso->chr_horafin == $temporal->chr_horafin && $curso->id != $temporal->id) {
                    $noprocede = 1;
                }
            }
        }
        if ($noprocede == 0) {
            global $USER;
            foreach ($array as $curso) {
                $obj = new \stdClass();
                $obj->int_matriculaid = $idmatricula;
                $obj->int_cursoid = $curso;
                //activo
                $obj->is_active = 1;
                //eliminado
                $obj->is_deleted = 0;
                $obj->int_creatorid = $USER->id;
                $obj->date_timecreated = time();
                MatriculaModel::saveMatriculaCurso($obj);
                $obj2 = \matricula\Model\CursoModel::getCursoById($curso);
                $obj2->is_ocupado = 1;
                $obj2->int_cantidad_ocupada = $obj2->int_cantidad_ocupada + 1;
                \matricula\Model\CursoModel::updateCurso($obj2);
            }
        }

        return $noprocede;
    }

    public function GuardarCompra($compraid, $efectivo, $delivery) {
        if ($delivery == 'false') {
            $dely = 0;
        } else {
            $dely = 15;
        }

        $items = $this->getAllCompraProductoByCompraid($compraid);
        $total = $this->getAllTotalCompraProductoByCompraid($compraid);
        $ServiceAlumno = new AlumnoService;
        global $USER;
        $obj = new \stdClass();
        $obj->id = $compraid;
        $obj->int_compra = $total - $efectivo;
         $obj->int_compra  = round(  $obj->int_compra ,2);
        $obj->int_total = $total - $efectivo + $dely;
        $obj->int_total  = round(  $obj->int_total ,2);
        $obj->int_efectivo = $efectivo;
        $obj->int_efectivo  = round(  $obj->int_efectivo ,2);
        $obj->int_delivery = $dely;
        $obj->is_active = 1;
        $obj->int_total_interes = $obj->int_total;
        $obj->int_total_interes  = round(  $obj->int_total_interes ,2);
        $fecha = MatriculaModel::getFechaActual();
        $obj->chr_fecha_registro = $fecha->chr_fecha_actual;
        $obj->chr_dia = $fecha->chr_dia;
        $obj->chr_mes = $fecha->chr_mes;
        $obj->chr_anio = $fecha->chr_anio;
        //eliminado
        $obj->is_deleted = 0;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        $obj->date_timecreated = time();
        $returnValue = MatriculaModel::updateCompra($obj);

        $compra = MatriculaModel::getCompraById($compraid);

        $credito = MatriculaModel::getMatriculaById($compra->int_credito_clienteid);
        $obj2 = new \stdClass();
        $obj2->id = $credito->id;
        $obj2->int_disponible = $credito->int_disponible -$obj->int_total;
        $obj2->int_disponible  = round($obj2->int_disponible ,2);
        $obj2->int_gastado = $credito->int_gastado + $obj->int_total;
        $obj2->int_gastado  = round($obj2->int_gastado ,2);
        $obj2->int_debe_monto = $credito->int_debe_monto + $obj->int_total;
        $obj2->int_debe_monto  = round($obj2->int_debe_monto ,2);
        
        MatriculaModel::updateMatricula($obj2);

        return $returnValue;
    }
    
    
    public function GuardarPagoMantenimiento($credito_cliente, $pago) {
        $ServiceAlumno = new AlumnoService;
        global $USER;
        $obj = new \stdClass();
        $obj->int_total = $pago;
        $obj->int_total = round($obj->int_total,2);
        $obj->int_credito_clienteid = $credito_cliente;
        
        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        $fecha = MatriculaModel::getFechaActual();
        $obj->chr_fecha_registro = $fecha->chr_fecha_actual;
        $obj->chr_dia = $fecha->chr_dia;
        $obj->chr_mes = $fecha->chr_mes;
        $obj->chr_anio = $fecha->chr_anio;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        $obj->date_timecreated = time();
        $returnValue = MatriculaModel::savePago($obj);

      
        $credito = MatriculaModel::getMatriculaById($credito_cliente);
        $obj2 = new \stdClass();
        $obj2->id = $credito->id;
        $obj2->int_pagado_monto = $credito->int_pagado_monto + $pago;
        $obj2->int_pagado_monto = round($obj2->int_pagado_monto,2);
        $obj2->int_debe_monto = $credito->int_debe_monto - $pago;
        $obj2->int_debe_monto = round($obj2->int_debe_monto,2);
        $obj2->int_mantenimiento = $credito->int_mantenimiento - $pago;
        $obj2->int_mantenimiento = round($obj2->int_mantenimiento,2);
        $obj2->int_disponible = $credito->int_disponible + $pago;
        $obj2->int_disponible = round($obj2->int_disponible,2);
        MatriculaModel::updateMatricula($obj2);
        return $returnValue;
    }
    

    public function GuardarPago($credito_cliente, $pago, $objcompra) {
        $ServiceAlumno = new AlumnoService;
        global $USER;
        $obj = new \stdClass();
        $obj->int_total = $pago;
         $obj->int_total = round($obj->int_total,2);
        $obj->int_credito_clienteid = $credito_cliente;
        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        $fecha = MatriculaModel::getFechaActual();
        $obj->chr_fecha_registro = $fecha->chr_fecha_actual;
        $obj->chr_dia = $fecha->chr_dia;
        $obj->chr_mes = $fecha->chr_mes;
        $obj->chr_anio = $fecha->chr_anio;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        $obj->date_timecreated = time();
        $returnValue = MatriculaModel::savePago($obj);

        $objcompra->int_total = $objcompra->int_total - $pago;
        $objcompra->int_total = round($objcompra->int_total,2);
        $objcompra->int_compra = $objcompra->int_compra - $pago;
        $objcompra->int_compra = round($objcompra->int_compra,2);
        $objcompra->int_efectivo = $objcompra->int_efectivo + $pago;
        $objcompra->int_efectivo = round($objcompra->int_efectivo,2);
        MatriculaModel::updateCompra($objcompra);
        $credito = MatriculaModel::getMatriculaById($credito_cliente);
        $obj2 = new \stdClass();
        $obj2->id = $credito->id;
        $obj2->int_pagado_monto = $credito->int_pagado_monto + $pago;
        $obj2->int_pagado_monto = round($obj2->int_pagado_monto,2);
        $obj2->int_debe_monto = $credito->int_debe_monto - $pago;
        $obj2->int_debe_monto = round($obj2->int_debe_monto,2);
        if ($objcompra->int_total == 0) {
            $obj2->int_disponible = $credito->int_disponible + $objcompra->int_total_interes;
            $obj2->int_disponible = round($obj2->int_disponible,2);
        }
        MatriculaModel::updateMatricula($obj2);
        return $returnValue;
    }

    public function GuardarCompraProducto($pro, $com, $can) {
        $ServiceAlumno = new AlumnoService;
        $producto = $ServiceAlumno->getProductoById($pro);
        global $USER;
        $obj = new \stdClass();
        $obj->int_compraid = $com;
        $obj->int_productoid = $pro;
        $obj->int_cantidad = $can;
        $obj->int_total = $can * $producto->int_precio;
        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        $obj->date_timecreated = time();
        $returnValue = MatriculaModel::saveCompraProducto($obj);
        return $returnValue;
    }

    public function CrearCompra($objid) {
        $id = $objid;
        global $USER;
        $obj = new \stdClass();

        $obj->is_active = 0;
        //eliminado
        $obj->is_deleted = 1;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        $obj->date_timecreated = time();
        $obj->int_credito_clienteid = $id;
        $returnValue = MatriculaModel::saveCompra($obj);
        return $returnValue;
    }

    public function GuardarMatricula($inputs) {

        $id = $inputs->get('matriculaid');
        global $USER;
        $obj = new \stdClass();
        //id
        $obj->id = $inputs->get('matriculaid');
        //nombre
        $obj->int_clienteid = $inputs->get('inputAlumno');
        $obj->int_credito_monto = $inputs->get('inputTxtDNI');
         $obj->int_credito_monto  = round($obj->int_credito_monto ,2);
        $obj->int_pagado_monto = 0;
        $obj->int_disponible = $obj->int_credito_monto - $obj->int_debe_monto;
        $obj->int_disponible  = round($obj->int_disponible ,2);
        
        $obj->chr_tipo_interes = $inputs->get('TipoTasaInteres');
        $obj->int_interes = $inputs->get('TasaInteres');
         $obj->int_interes  = round($obj->int_interes ,2);
        $obj->chr_periodo = $inputs->get('PeriodoInteres');
        $obj->int_capitalizacion = $inputs->get('Capitalizacion');
        $obj->int_dias = $inputs->get('DiasPeriodo');
        $obj->int_dias_totales = $inputs->get('DiasPeriodo');
        $fecha = MatriculaModel::getFechaActual();
        $obj->chr_fecha_registro = $fecha->chr_fecha_actual;
        $obj->chr_dia = $fecha->chr_dia;
        $obj->chr_mes = $fecha->chr_mes;
        $obj->chr_anio = $fecha->chr_anio;
        //Kevin tasa efectiva formula
        $obj->int_tasa_efectiva = (pow((1 + (($obj->int_interes / 100) / 30)), $obj->int_dias) - 1);
//       $obj->int_tasa_efectiva  = round($obj->int_tasa_efectiva ,2);

        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;

        $objA = new \stdClass();
        $objA->id = $inputs->get('inputAlumno');
        $objA->is_ocupado = 1;
        \matricula\Model\AlumnoModel::updateAlumno($objA);
        if ($id > 0) {
            //fecha modificacion registro
            $obj->date_timemodified = time();
            $returnValue = MatriculaModel::updateMatricula($obj);
        } else {
            //fecha creacion registro
            $obj->date_timecreated = time();
            $returnValue = MatriculaModel::saveMatricula($obj);
        }


        $returnValue = $inputs->get('matriculaid');
        return $returnValue;
    }

}
