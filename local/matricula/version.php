<?php

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2018011213; // The (date) version of this plugin
$plugin->maturity = MATURITY_STABLE;
$plugin->release   = '3.5+ (Build: 20171208)';
$plugin->requires  = 2016120500; // Requires this Moodle version
$plugin->component = 'local_matricula';

// Requiere campus para los jss y css.
//$plugin->dependencies = ['theme_campus' => 2017042000];