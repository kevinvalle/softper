<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Controller;

use matricula\Service\AlumnoService;
use matricula\Service\UtilService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use core\output\notification;
use context_system;
use moodle_url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class AlumnoController extends AlumnoService {

    private $params;
    private $utilService;

    public function __construct() {
        $this->params = [];
    }

    /**
     * Vista para listar los registros de la tabla sílabos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        //obtenemos las solicitudes
        $alumnos = $this->getAlumnosAll();
        //$modalidades = $this->getModalidadesMenu();
        $this->params['alumnos'] = $alumnos;
        $this->params['url_ajax'] = $this->routes()->generate('alumnos_ajax');
        $this->params['url_nuevo_alumno'] = $this->routes()->generate('alumnos_editar');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Alumno')->renderResponse('index.html.twig', $this->params);
    }

    public function editarAction(Request $request) {
        $this->params['title'] = "REGISTRAR NUEVO PRODUCTO";
        //obtenemos el id
        $id = $request->attributes->get('alumnoid');
        //obtenemos los ciclos
        $ciclos = array("Salados", "Dulces");
        //obtenemos el alumno
        if ($id > 0) {
            $objAlumno = $this->getProductoById($id);
        }
        //parametros
        $this->params['objAlumno'] = $objAlumno;
        $this->params['ciclos'] = $ciclos;
        $this->params['url_ajax'] = $this->routes()->generate('alumnos_ajax');
        $this->params['url_home'] = $this->routes()->generate('index');
        $this->params['url_backto'] = $this->routes()->generate('alumnos_index');
        return $this->template('Alumno')->renderResponse('editar.html.twig', $this->params);
    }

    public function ajaxAction(Request $request) {
        global $USER;
        $subject = $request->request->get('subject');
        $this->utilService = new UtilService();
        $error = 0;
        $message = '';
        switch ($subject) {
            case 'GuardarAlumno':
                $inputs = $request->request;
                $id = $inputs->get('inputId');
                $name = $inputs->get('inputTxtName');
                $tipo = $inputs->get('TipoProducto');
                $precio = $inputs->get('Precio');
                 if($name!=" " &&$name!="" && $tipo!=" " &&$tipo!="" &&$precio!=" " &&$precio!=""){
                        $solicitudid = $this->GuardarAlumno($inputs);
                }else{
                     $error=true;
                     $message='Complete todos los campos para continuar';
                 }
                $response['data'] = [
                    'id' => $solicitudid,
                    'url' => $this->routes()->generate('alumnos_index')
                ];
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteAlumno':
                $alumnoid = $request->request->get('alumnoid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarAlumno($alumnoid);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
        }
    }

    public function build_source_field($source) {
        $sourcefield = new \stdClass();
        $sourcefield->source = $source;
        return serialize($sourcefield);
    }

}
