<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Core;

use moodle_url;
use matricula\Resources\config\Routes;
use matricula\Service\HomeService;
use matricula\Service\AlumnoService;
use matricula\Service\CursoService;
use matricula\Service\MatriculaService;
use matricula\Service\DocenteService;
use matricula\Service\UtilService;
use matricula\Resources\config\View;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Templating\DelegatingEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Twig_Environment;
use Twig_Loader_Filesystem;

class Template {

    public function template($controller) {
        $viewDirroot = View::getCollection($controller);

        $loader = new Twig_Loader_Filesystem($viewDirroot);
        $twig = new Twig_Environment($loader);

        // Add global services here
        $twig->addGlobal('home', new HomeService());
        $twig->addGlobal('alumno', new AlumnoService());
        $twig->addGlobal('curso', new CursoService());
        $twig->addGlobal('docente', new DocenteService());
        $twig->addGlobal('matricula', new MatriculaService());
        $twig->addGlobal('util', new UtilService());
        $templating = new TwigEngine($twig, new TemplateNameParser(), new FileLocator($viewDirroot));

        return $templating;
    }

    public function routes() {
        $routes = Routes::getCollection();

        $context = new RequestContext();
        $url = new moodle_url('/local/matricula');
        $context->setBaseUrl($url);

        $generator = new UrlGenerator($routes, $context);

        return $generator;
    }

    public function render($view, $params) {
        global $CFG;

        $params = json_decode(json_encode($params), true);

        $view = explode(':', $view);

        $viewController = $view[0];
        $viewFile = $view[1];

        $viewDirroot = View::getCollection($viewController);

        $loader = new Twig_Loader_Filesystem($viewDirroot);
        $twig = new Twig_Environment($loader);

        $templating = new DelegatingEngine([new TwigEngine(new Twig_Environment(new Twig_Loader_Filesystem($viewDirroot)), new TemplateNameParser(), new FileLocator(View::getCollection($viewController)/* "$CFG->dirroot/my/Resources/views/Home/" */))]);
        $template = $twig->load($viewFile);

        ob_start();

        echo $template->render($params);

        return new Response(ob_get_clean());
    }

}
